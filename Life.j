.source Life.j
.class public Life
.super java/lang/Object
.method public <init>()V
.limit stack 1
.limit locals 1
.var 0 is 'this' LLife;
	aload 0
	invokespecial java/lang/Object/<init>()V
	return
.end method
.field public UNDERPOP_LIM I
.field public OVERPOP_LIM I
.field public REPRODUCE_NUM I
.field public LOOPS_PER_MS I
.field public xMax I
.field public yMax I
.field public field [I
.method public static main([Ljava/lang/String;)V
.method public init()Z
.var 0 is 'this'LLife;
.var 1 is 'lineLenA'[I
.var 2 is 'lineLen'I
	istore 1
	istore null
	istore null
	istore null
	istore null
	istore null
	istore 2
	iload 2
	isub
	istore null
	iload 2
	idiv
	isub
	istore null
.end method
.method public field([I)[I
.var 0 is 'this'LLife;
.var 1 is 'lineLen'[I
.var 2 is 'field'[I
	istore 2
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
.end method
.method public update()Z
.var 0 is 'this'LLife;
.var 1 is 'i'I
.var 2 is 'cur'I
.var 3 is 'neighN'I
.var 4 is 'goodPop'Z
.var 5 is 'newField'[I
	istore 5
	istore 1
	iload 5
	istore null
.end method
.method public printField()Z
.var 0 is 'this'LLife;
.var 1 is 'i'I
.var 2 is 'j'I
	istore 1
	istore 2
.end method
.method public trIdx(I,I)I
.var 0 is 'this'LLife;
.var 1 is 'x'I
.var 2 is 'y'I
.end method
.method public cartIdx(I)[I
.var 0 is 'this'LLife;
.var 1 is 'absPos'I
.var 2 is 'x'I
.var 3 is 'y'I
.var 4 is 'xLim'I
.var 5 is 'ret'[I
	iload null
	iadd
	istore 4
	iload 1
	iload 4
	idiv
	istore 3
	iload 1
	iload 3
	iload 4
	iprod
	isub
	istore 2
	istore 5
	iload 2
	istore null
	iload 3
	istore null
.end method
.method public getNeighborCoords(I)[I
.var 0 is 'this'LLife;
.var 1 is 'absPos'I
.var 2 is 'x'I
.var 3 is 'y'I
.var 4 is 'upX'I
.var 5 is 'upY'I
.var 6 is 'downX'I
.var 7 is 'downY'I
.var 8 is 'cart'[I
.var 9 is 'ret'[I
	istore 8
	istore 2
	istore 3
	istore 9
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
	istore null
.end method
.method public getLiveNeighborN(I)I
.var 0 is 'this'LLife;
.var 1 is 'absPos'I
.var 2 is 'neigh'[I
.var 3 is 'i'I
.var 4 is 'ret'I
	istore 4
	istore 2
	istore 3
.end method
.method public busyWait(I)Z
.var 0 is 'this'LLife;
.var 1 is 'ms'I
.var 2 is 'i'I
.var 3 is 'n'I
	iload 1
	iload null
	iprod
	istore 3
	istore 2
.end method
.method public eq(I,I)Z
.var 0 is 'this'LLife;
.var 1 is 'a'I
.var 2 is 'b'I
.end method
.method public ne(I,I)Z
.var 0 is 'this'LLife;
.var 1 is 'a'I
.var 2 is 'b'I
.end method
.method public lt(I,I)Z
.var 0 is 'this'LLife;
.var 1 is 'a'I
.var 2 is 'b'I
.end method
.method public le(I,I)Z
.var 0 is 'this'LLife;
.var 1 is 'a'I
.var 2 is 'b'I
.end method
.method public gt(I,I)Z
.var 0 is 'this'LLife;
.var 1 is 'a'I
.var 2 is 'b'I
.end method
.method public ge(I,I)Z
.var 0 is 'this'LLife;
.var 1 is 'a'I
.var 2 is 'b'I
.end method
