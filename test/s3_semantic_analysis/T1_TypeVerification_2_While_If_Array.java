package s3_semantic_analysis;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.ParserTest;

public class T1_TypeVerification_2_While_If_Array {

    @Test
    public void While_If_Array_WhileCondBool() {
		(new ParserTest()).test("s3_semantic_analysis/type_verification/While_If_Array_WhileCondBool.jmm", false);
    }
	
    @Test
    public void While_If_Array_WhileCondNotBool() {
		(new ParserTest()).test("s3_semantic_analysis/type_verification/While_If_Array_WhileCondNotBool.jmm", true);
    }
	
    @Test
    public void While_If_Array_IfCondBool() {
		(new ParserTest()).test("s3_semantic_analysis/type_verification/While_If_Array_IfCondBool.jmm", false);
    }
	
    @Test
    public void While_If_Array_IfCondNotBool() {
		(new ParserTest()).test("s3_semantic_analysis/type_verification/While_If_Array_IfCondNotBool.jmm", true);
    }
	
    @Test
    public void While_If_Array_AccessArrayInteger() {
		(new ParserTest()).test("s3_semantic_analysis/type_verification/While_If_Array_AccessArrayInteger.jmm", false);
    }
	
    @Test
    public void While_If_Array_AccessArrayNotInteger() {
		(new ParserTest()).test("s3_semantic_analysis/type_verification/While_If_Array_AccessArrayNotInteger.jmm", true);
    }	


}
