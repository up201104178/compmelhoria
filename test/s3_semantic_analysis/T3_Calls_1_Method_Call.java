package s3_semantic_analysis;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.ParserTest;

public class T3_Calls_1_Method_Call {


    @Test
    public void Method_Call_Simple() {
        (new ParserTest()).test("s3_semantic_analysis/calls/Method_Call_Simple.jmm", false);
    }


    @Test
    public void Method_Call_Missing() {
        (new ParserTest()).test("s3_semantic_analysis/calls/Method_Call_Missing.jmm", true);
    }


}
