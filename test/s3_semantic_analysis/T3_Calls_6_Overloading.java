package s3_semantic_analysis;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.ParserTest;

public class T3_Calls_6_Overloading {


    @Test
    public void Overloading_Declared_Wrong_Type() {
        (new ParserTest()).test("s3_semantic_analysis/calls/Overloading_Declared_Wrong_Type.jmm", true);
    }

    @Test
    public void Overloading_Declared_Wrong_Number() {
        (new ParserTest()).test("s3_semantic_analysis/calls/Overloading_Declared_Wrong_Number.jmm", true);
    }

    @Test
    public void Overloading_Imported_Wrong_Type() {
        (new ParserTest()).test("s3_semantic_analysis/calls/Overloading_Imported_Wrong_Type.jmm", true);
    }

    @Test
    public void Overloading_Imported_Wrong_Number() {
        (new ParserTest()).test("s3_semantic_analysis/calls/Overloading_Imported_Wrong_Number.jmm", true);
    }
	



}
