package s3_semantic_analysis;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.ParserTest;

public class T1_TypeVerification_5_Array {

    @Test
    public void arrayAccessOnInt() {
		(new ParserTest()).test("s3_semantic_analysis/type_verification/Array_Access_On_Int.jmm", true);
    }
	
    @Test
    public void arraySumBad() {
		(new ParserTest()).test("s3_semantic_analysis/type_verification/Array_Sum_Bad.jmm", true);
    }


}
