package s3_semantic_analysis;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.ParserTest;

public class T3_Calls_3_Other_Class_Method_Call {


    @Test
    public void Other_Class_Method_Call_Simple() {
        (new ParserTest()).test("s3_semantic_analysis/calls/Other_Class_Method_Call_Simple.jmm", false);
    }


    @Test
    public void Other_Class_Method_Call_Missing_Import() {
        (new ParserTest()).test("s3_semantic_analysis/calls/Other_Class_Method_Call_Missing_Import.jmm", true);
    }
}
