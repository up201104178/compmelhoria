package s3_semantic_analysis;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.ParserTest;



public class T1_TypeVerification_1_BinaryOps {


    @Test
    public void BinaryOps_Simple() {
		(new ParserTest()).test("s3_semantic_analysis/type_verification/BinaryOps_Simple.jmm", false);
    }
	
    @Test
    public void BinaryOps_BadInts() {
		(new ParserTest()).test("s3_semantic_analysis/type_verification/BinaryOps_BadInts.jmm", true);
    }
	
    @Test
    public void BinaryOps_BadBools() {
		(new ParserTest()).test("s3_semantic_analysis/type_verification/BinaryOps_BadBools.jmm", true);
    }

}
