package s3_semantic_analysis;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.ParserTest;

public class T1_TypeVerification_4_Assignment {

    @Test
    public void Assignment_Correct() {
		(new ParserTest()).test("s3_semantic_analysis/type_verification/Assignment_Correct.jmm", false);
    }
	
    @Test
    public void Assignment_Array_Bad() {
		(new ParserTest()).test("s3_semantic_analysis/type_verification/Assignment_Array_Bad.jmm", true);
    }

	@Test
    public void Assignment_Bool_Bad() {
		(new ParserTest()).test("s3_semantic_analysis/type_verification/Assignment_Bool_Bad.jmm", true);
    }

	@Test
    public void Assignment_Int_Bad() {
		(new ParserTest()).test("s3_semantic_analysis/type_verification/Assignment_Int_Bad.jmm", true);
    }


}
