package s3_semantic_analysis;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.ParserTest;

public class T1_TypeVerification_3_Variable_Undefined {

    @Test
    public void Variable_Undefined_Array() {
		(new ParserTest()).test("s3_semantic_analysis/type_verification/Variable_Undefined_Array.jmm", true);
    }
	
    @Test
    public void Variable_Undefined_Int() {
		(new ParserTest()).test("s3_semantic_analysis/type_verification/Variable_Undefined_Int.jmm", true);
    }
	
    @Test
    public void Variable_Undefined_Boolean() {
		(new ParserTest()).test("s3_semantic_analysis/type_verification/Variable_Undefined_Boolean.jmm", true);
    }
	


}
