package s3_semantic_analysis;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.ParserTest;

public class T3_Calls_4_Instance_And_Static {


    @Test
    public void Instance_And_Static_Call_Static_On_Instance() {
        (new ParserTest()).test("s3_semantic_analysis/calls/Instance_And_Static_Call_Static_On_Instance.jmm", false);
    }


    @Test
    public void Instance_And_Static_Call_Instance_On_Static() {
        (new ParserTest()).test("s3_semantic_analysis/calls/Instance_And_Static_Call_Instance_On_Static.jmm", true);
    }
}
