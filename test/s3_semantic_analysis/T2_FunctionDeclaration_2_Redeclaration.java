package s3_semantic_analysis;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.ParserTest;

public class T2_FunctionDeclaration_2_Redeclaration {


    @Test
    public void Redeclaration_Same() {
        (new ParserTest()).test("s3_semantic_analysis/function_declaration/Redeclaration_Same.jmm", true);
    }
	
    @Test
    public void Redeclaration_Return_Diff() {
        (new ParserTest()).test("s3_semantic_analysis/function_declaration/Redeclaration_Return_Diff.jmm", true);
    }

}
