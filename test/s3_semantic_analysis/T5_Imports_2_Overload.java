package s3_semantic_analysis;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.ParserTest;

public class T5_Imports_2_Overload {

    @Test
    public void Overload_Import() {
        (new ParserTest()).test("s3_semantic_analysis/import/Overload_Import.jmm", false);
    }

    @Test
    public void Overload_Import_Missing() {
        (new ParserTest()).test("s3_semantic_analysis/import/Overload_Import_Missing.jmm", true);
    }

   

}
