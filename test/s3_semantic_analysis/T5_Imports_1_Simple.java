package s3_semantic_analysis;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.ParserTest;

public class T5_Imports_1_Simple {

    @Test
    public void Simple_Import() {
        (new ParserTest()).test("s3_semantic_analysis/import/Simple_Import.jmm", false);
    }

    @Test
    public void Simple_Import_Missing() {
        (new ParserTest()).test("s3_semantic_analysis/import/Simple_Import_Missing.jmm", true);
    }

   

}
