package s3_semantic_analysis;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.ParserTest;

public class T2_FunctionDeclaration_1_Overloading {


    @Test
    public void Overloading_Simple() {
        (new ParserTest()).test("s3_semantic_analysis/function_declaration/Overloading_Simple.jmm", false);
    }

    @Test
    public void Overloading_Diff_Args_Same_Number() {
        (new ParserTest()).test("s3_semantic_analysis/function_declaration/Overloading_Diff_Args_Same_Number.jmm", false);
    }

}
