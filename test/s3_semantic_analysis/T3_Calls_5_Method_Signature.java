package s3_semantic_analysis;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.ParserTest;

public class T3_Calls_5_Method_Signature {


    @Test
    public void Method_Signature_Simple() {
        (new ParserTest()).test("s3_semantic_analysis/calls/Method_Signature_Simple.jmm", false);
    }


    @Test
    public void Method_Signature_Wrong_Types() {
        (new ParserTest()).test("s3_semantic_analysis/calls/Method_Signature_Wrong_Types.jmm", true);
    }

    @Test
    public void Method_Signature_Wrong_Number() {
        (new ParserTest()).test("s3_semantic_analysis/calls/Method_Signature_Wrong_Number.jmm", true);
    }
	
    @Test
    public void Method_Signature_Wrong_Literal_Type() {
        (new ParserTest()).test("s3_semantic_analysis/calls/Method_Signature_Wrong_Literal_Type.jmm", true);
    }


}
