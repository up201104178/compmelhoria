package s4_code_generation;

import static org.junit.Assert.*;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.CodeGeneratorUtils;


public class T1_Basic_3_Field {

public static String getFieldRegex(String varName, String varType) {

	return "\\.field\\s+((public|private)\\s+)?'?\\w*?"+varName+"\\w*?'?\\s+'?"+varType+"'?";
}

/*checks if field declaration is correct (int)*/
@Test
public void Field_Declaration_Int() {
    String jCode = JasminUtils.testJmm("s4_code_generation/basic/BasicFields.jmm", null, false);
    /*
	boolean contains = CodeGeneratorUtils.codeContainsExpression(jCode, new String[]{
        ".field public 'a' I", ".field public a I", ".field a I", ".field 'a' I", ".field private 'a' I", ".field private a I",
        ".field public '_a' I", ".field public _a I", ".field _a I", ".field '_a' I", ".field private '_a' I", ".field private _a I"
        });
		*/
    boolean contains = CodeGeneratorUtils.codeContainsExpressionRegex(jCode, getFieldRegex("a", "I"));		
		
    assertTrue("failed with code: " + jCode, contains);
}

/*checks if field declaration is correct (boolean)*/
@Test
public void Field_Declaration_Boolean() {
    String jCode = JasminUtils.testJmm("s4_code_generation/basic/BasicFields.jmm", null, false);
    /*
	boolean contains = CodeGeneratorUtils.codeContainsExpression(jCode, new String[]{
        ".field public 'b' Z", ".field public b Z", ".field b Z", ".field 'b' Z", ".field private 'b' Z", ".field private b Z",
        ".field public '_b' Z", ".field public _b Z", ".field _b Z", ".field '_b' Z", ".field private '_b' Z", ".field private _b Z"
         });
		*/ 
	boolean contains = CodeGeneratorUtils.codeContainsExpressionRegex(jCode, getFieldRegex("b", "Z"));	 
    assertTrue("failed with code: " + jCode, contains);
}

/*checks if field declaration is correct (class)*/
@Test
public void Field_Declaration_Class() {
    String jCode = JasminUtils.testJmm("s4_code_generation/basic/BasicFields.jmm", null, false);
	/*
    boolean contains = CodeGeneratorUtils.codeContainsExpression(jCode, new String[]{
        ".field public 'p' 'LBasicFields;'", ".field public 'p' LBasicFields;", ".field public p LBasicFields;", ".field public p 'LBasicFields;'", ".field p LBasicFields;", ".field p 'LBasicFields;'", ".field 'p' LBasicFields;", ".field 'p' 'LBasicFields;'", ".field private 'p' LBasicFields;", ".field private 'p' 'LBasicFields;'", ".field private p LBasicFields;", ".field private p 'LBasicFields;'",
        ".field public '_p' 'LBasicFields;'", ".field public '_p' LBasicFields;", ".field public _p LBasicFields;", ".field public _p 'LBasicFields;'", ".field _p LBasicFields;", ".field _p 'LBasicFields;'", ".field '_p' LBasicFields;", ".field '_p' 'LBasicFields;'", ".field private '_p' LBasicFields;", ".field private '_p' 'LBasicFields;'", ".field private _p LBasicFields;", ".field private _p 'LBasicFields;'"

        });
	*/	
	boolean contains = CodeGeneratorUtils.codeContainsExpressionRegex(jCode, getFieldRegex("p", "LBasicFields;"));	 	
    assertTrue("failed with code: " + jCode, contains);
}

/*checks if field declaration is correct (array)*/
@Test
public void Field_Declaration_Array() {
    String jCode = JasminUtils.testJmm("s4_code_generation/basic/BasicFields.jmm", null, false);
    /*
	boolean contains = CodeGeneratorUtils.codeContainsExpression(jCode, new String[]{
        ".field public 'c' [I", ".field public c [I", ".field c [I", ".field 'c' [I", ".field private 'c' [I", ".field private c [I",
        ".field public '_c' [I", ".field public _c [I", ".field _c [I", ".field '_c' [I", ".field private '_c' [I", ".field private _c [I"

    });
	*/
	boolean contains = CodeGeneratorUtils.codeContainsExpressionRegex(jCode, getFieldRegex("c", "\\[I"));		
    assertTrue("failed with code: " + jCode, contains);
}


}
