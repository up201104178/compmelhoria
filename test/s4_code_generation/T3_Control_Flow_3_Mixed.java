package s4_code_generation;

import static org.junit.Assert.*;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.CodeGeneratorUtils;


public class T3_Control_Flow_3_Mixed {


/*checks if the code of a more complex IF ELSE statement (similar a switch statement) is well executed */
@Test
public void Mixed_Switch() {
    JasminUtils.testJmm("s4_code_generation/control_flow/SwitchStat.jmm", "Result: 1\nResult: 2\nResult: 3\nResult: 4\nResult: 5\nResult: 6\nResult: 7");
}

/*checks if the code of a more complex program is well executed. Multiples nested WHILE and IF ELSE statements */
@Test
public void Mixed_Complex() {
    JasminUtils.testJmm("s4_code_generation/control_flow/ComplexStat.jmm", "Result: 1\nResult: 2\nResult: 3\nResult: 3\nResult: 4\nResult: 1\nResult: 2");
}


}
