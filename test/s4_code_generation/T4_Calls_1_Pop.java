package s4_code_generation;

import static org.junit.Assert.*;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.CodeGeneratorUtils;


public class T4_Calls_1_Pop {


/*checks if pop is being used when required*/
@Test
public void UsesPop() {
    String jCode = JasminUtils.testJmm("s4_code_generation/calls/UsesPop.jmm", null, false);
    boolean contains = CodeGeneratorUtils.methodContainsExpression(jCode,"func5", new String[]{"pop"});
    assertTrue(contains);
}



}
