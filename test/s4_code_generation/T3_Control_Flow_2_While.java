package s4_code_generation;

import static org.junit.Assert.*;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.CodeGeneratorUtils;


public class T3_Control_Flow_2_While {



/*checks if the code of a simple WHILE statement is well executed */
@Test
public void While_Simple() {
    JasminUtils.testJmm("s4_code_generation/control_flow/SimpleWhileStat.jmm", "Result: 0\nResult: 1\nResult: 2");
}


}
