package s4_code_generation;

import static org.junit.Assert.*;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.CodeGeneratorUtils;


public class T2_Arithmetic_3_Complex {


/*checks if an addition is correct (more than 2 values)*/
@Test
public void Complex_Add() {
    JasminUtils.testJmm("s4_code_generation/arithmetic/ComplexAdd.jmm", "Result: 19");
}

/*checks if a subtraction is correct (more than 2 values)*/
@Test
public void Complex_Sub() {
    JasminUtils.testJmm("s4_code_generation/arithmetic/ComplexSub.jmm", "Result: 5");
}

/*checks if a multiplication is correct (more than 2 values)*/
@Test
public void Complex_Prod() {
    JasminUtils.testJmm("s4_code_generation/arithmetic/ComplexProd.jmm", "Result: 196");
}

/*checks if a division is correct (more than 2 values)*/
@Test
public void Complex_Div() {
    JasminUtils.testJmm("s4_code_generation/arithmetic/ComplexDiv.jmm", "Result: 1");
}

/*checks if a combination of addition and subtraction is correct*/
@Test
public void Complex_AddSub() {
    JasminUtils.testJmm("s4_code_generation/arithmetic/ComplexAddSub.jmm", "Result: 7");
}

/*checks if a combination of addition and multiplication is correct (checks precedences)*/
@Test
public void Complex_AddMul() {
    JasminUtils.testJmm("s4_code_generation/arithmetic/ComplexAddMul.jmm", "Result: 73");
}

/*checks if a combination of addition and division is correct (checks precedences)*/
@Test
public void Complex_AddDiv() {
    JasminUtils.testJmm("s4_code_generation/arithmetic/ComplexAddDiv.jmm", "Result: 10");
}

/*checks if a combination of subtraction and multiplication is correct (checks precedences)*/
@Test
public void Complex_SubMul() {
    JasminUtils.testJmm("s4_code_generation/arithmetic/ComplexSubMul.jmm", "Result: 19");
}

/*checks if a combination of subtraction and division is correct (checks precedences)*/
@Test
public void Complex_SubDiv() {
    JasminUtils.testJmm("s4_code_generation/arithmetic/ComplexSubDiv.jmm", "Result: 5");
}

/*checks if a combination of multiplication and division is correct (checks precedences)*/
@Test
public void Complex_MulDiv() {
    JasminUtils.testJmm("s4_code_generation/arithmetic/ComplexMulDiv.jmm", "Result: 2");
}

/*checks if a combination of multiplication and division is correct (checks precedences)*/
@Test
public void Complex_DivMul() {
    JasminUtils.testJmm("s4_code_generation/arithmetic/ComplexDivMul.jmm", "Result: 8");
}

/*checks if a more complex arithmetic expression (multiple different operations) is correct*/
@Test
public void Complex_Arithmetic() {
    JasminUtils.testJmm("s4_code_generation/arithmetic/ComplexArithmetic.jmm", "Result: 168");
}



}
