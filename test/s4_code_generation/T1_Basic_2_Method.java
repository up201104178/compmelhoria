package s4_code_generation;

import static org.junit.Assert.*;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.CodeGeneratorUtils;


public class T1_Basic_2_Method {



/*checks if method declaration is correct (int)*/
@Test
public void Method_Declaration_Int() {
    String jCode = JasminUtils.testJmm("s4_code_generation/basic/BasicMethods.jmm", null, false);
    boolean contains = CodeGeneratorUtils.codeContainsExpression(jCode, new String[]{".method public func1()I", ".method private func1()I"});
    assertTrue("failed with code: " + jCode, contains);
}

/*checks if method declaration is correct (boolean)*/
@Test
public void Method_Declaration_Boolean() {
    String jCode = JasminUtils.testJmm("s4_code_generation/basic/BasicMethods.jmm", null, false);
    boolean contains = CodeGeneratorUtils.codeContainsExpression(jCode, new String[]{".method public func2()Z", ".method private func2()Z"});
    assertTrue("failed with code: " + jCode, contains);
}

/*checks if method declaration is correct (class)*/
@Test
public void Method_Declaration_Class() {
    String jCode = JasminUtils.testJmm("s4_code_generation/basic/BasicMethods.jmm", null, false);
    boolean contains = CodeGeneratorUtils.codeContainsExpression(jCode, new String[]{".method public func3()'LBasicMethods;'", ".method public func3()LBasicMethods;", ".method private func3()'LBasicMethods;'", ".method private func3()LBasicMethods;"});
    assertTrue("failed with code: " + jCode, contains);
}

/*checks if method declaration is correct (array)*/
@Test
public void Method_Declaration_Array() {
    String jCode = JasminUtils.testJmm("s4_code_generation/basic/BasicMethods.jmm", null, false);
    boolean contains = CodeGeneratorUtils.codeContainsExpression(jCode, new String[]{".method public func4()[I", ".method private func4()[I"});
    assertTrue("failed with code: " + jCode, contains);
}


/*checks if main declaration is correct*/
@Test
public void Method_Declaration_Main() {
    String jCode = JasminUtils.testJmm("s4_code_generation/basic/BasicMethods.jmm", null, false);
    boolean contains = CodeGeneratorUtils.codeContainsExpression(jCode, new String[]{".method public static main([Ljava/lang/String;)V", ".method static public main([Ljava/lang/String;)V"});
    assertTrue("failed with code: " + jCode, contains);
}


}
