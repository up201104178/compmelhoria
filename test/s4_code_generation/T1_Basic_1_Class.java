package s4_code_generation;

import static org.junit.Assert.*;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.CodeGeneratorUtils;


public class T1_Basic_1_Class {

/*checks if class declaration is correct*/
@Test
public void Class_Declaration() {
    String jCode = JasminUtils.testJmm("s4_code_generation/basic/BasicMethods.jmm", null, false);
    boolean contains = CodeGeneratorUtils.codeContainsExpression(jCode, new String[]{".class public 'BasicMethods'", ".class public BasicMethods"});
    assertTrue("failed with code: " + jCode, contains);
}

/*checks if extends declaration is correct*/
@Test
public void Class_Declaration_With_Extends() {
    String jCode = JasminUtils.testJmm("s4_code_generation/basic/BasicMethods.jmm", null, false);
    boolean contains = CodeGeneratorUtils.codeContainsExpression(jCode, new String[]{".super 'Other'", ".super Other"});
    assertTrue("failed with code: " + jCode, contains);
}


/*checks if class init is correct*/
@Test
public void Class_Init() {
    String jCode = JasminUtils.testJmm("s4_code_generation/basic/BasicMethods.jmm", null, false);
    Boolean containsAload = CodeGeneratorUtils.methodContainsExpression(jCode, "<init>", new String[]{"aload_0", "aload 0"});
    Boolean containsInvoke = CodeGeneratorUtils.methodContainsExpression(jCode, "<init>", new String[]{"invokenonvirtual Other/<init>()V", "invokespecial Other/<init>()V"});
    Boolean containsReturn = CodeGeneratorUtils.methodContainsExpression(jCode, "<init>", new String[]{"return"});
    assertTrue("failed with code: " + jCode, containsAload && containsInvoke && containsReturn);


}

}
