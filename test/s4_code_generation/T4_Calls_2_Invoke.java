package s4_code_generation;

import static org.junit.Assert.*;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.CodeGeneratorUtils;


public class T4_Calls_2_Invoke {



/*checks if dynamic invocations work properly*/
@Test
public void Invoke_Virtual() {
    String jCode = JasminUtils.testJmm("s4_code_generation/calls/NoArgsFuncCall.jmm", null, false);
    boolean contains = CodeGeneratorUtils.methodContainsExpression(jCode,"func2", new String[]{"invokevirtual NoArgsFuncCall/func", "invokevirtual NoArgsFuncCall.func"});
    assertTrue(contains);
}

/*checks if static invocations work properly*/
@Test
public void Invoke_Static() {
    String jCode = JasminUtils.testJmm("s4_code_generation/calls/InvokeStatic.jmm", null, false);
    boolean contains = CodeGeneratorUtils.methodContainsExpression(jCode,"func", new String[]{"invokestatic ioPlus/printResult", "invokestatic ioPlus.printResult"});
    assertTrue(contains);
}

}
