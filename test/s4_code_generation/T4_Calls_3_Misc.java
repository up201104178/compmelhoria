package s4_code_generation;

import static org.junit.Assert.*;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.CodeGeneratorUtils;


public class T4_Calls_3_Misc {


/*checks if the code of a call to a function without arguments is well executed*/
@Test
public void Misc_NoArgs() {
    String jCode = JasminUtils.testJmm("s4_code_generation/calls/NoArgsFuncCall.jmm", null, false);
    boolean contains = CodeGeneratorUtils.methodContainsExpression(jCode,"func2", new String[]{"invokevirtual NoArgsFuncCall"});
    assertTrue(contains);
}

/*checks if the code of a call to a function with one argument is well executed*/
@Test
public void Misc_OneArg() {
    JasminUtils.testJmm("s4_code_generation/calls/OneArgFuncCall.jmm", "Result: 10");
}

/*checks if the code of a call to a function with multiple arguments (using constants in the call) is well executed*/
@Test
public void Misc_ConstArgs() {
    JasminUtils.testJmm("s4_code_generation/calls/ConstArgsFuncCall.jmm", "Result: 10\nResult: 12\nResult: 11");
}

/*checks if the code of a call to a function with multiple arguments (using variables in the call) is well executed*/
@Test
public void Misc_VarArgs() {
    JasminUtils.testJmm("s4_code_generation/calls/VarArgsFuncCall.jmm", "Result: 10\nResult: 12\nResult: 11");
}

/*checks if the code of a call to a function with multiple arguments (using arithmetic expressions in the call) is well executed*/
@Test
public void Misc_ArithmeticArgs() {
    JasminUtils.testJmm("s4_code_generation/calls/ArithmeticArgsFuncCall.jmm", "Result: 0\nResult: 15\nResult: 5");
}

/*checks if the code of a call to a function with multiple arguments (using boolean expressions in the call) is well executed*/
@Test
public void Misc_ConditionArgs() {
    JasminUtils.testJmm("s4_code_generation/calls/ConditionArgsFuncCall.jmm", "Result: 10");
}

/*checks if the code of a call to a function with multiple arguments (using other calls to functions in the call) is well executed*/
@Test
public void Misc_FunctionArgs() {
    JasminUtils.testJmm("s4_code_generation/calls/FuncArgsFuncCall.jmm", "Result: 10\nResult: 5\n");
}

/*checks if the code of a call to a function with multiple arguments (using complex expressions in the call) is well executed*/
@Test
public void Misc_ComplexArgs() {
    JasminUtils.testJmm("s4_code_generation/calls/ComplexArgsFuncCall.jmm", "Result: 10\nResult: 41\n");
}

}
