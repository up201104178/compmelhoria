package s4_code_generation;

import static org.junit.Assert.*;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.CodeGeneratorUtils;


public class T2_Arithmetic_2_Simple {




/*checks if a simple addition is correct (only with 2 values)*/
@Test
public void Simple_Add() {
    JasminUtils.testJmm("s4_code_generation/arithmetic/SimpleAdd.jmm", "Result: 8");
}

/*checks if a simple subtraction is correct (only with 2 values)*/
@Test
public void Simple_Sub() {
    JasminUtils.testJmm("s4_code_generation/arithmetic/SimpleSub.jmm", "Result: 6");
}

/*checks if a simple multiplication is correct (only with 2 values)*/
@Test
public void Simple_Prod() {
    JasminUtils.testJmm("s4_code_generation/arithmetic/SimpleProd.jmm", "Result: 7");
}

/*checks if a simple divsion is correct (only with 2 values)*/
@Test
public void Simple_Div() {
    JasminUtils.testJmm("s4_code_generation/arithmetic/SimpleDiv.jmm", "Result: 5");
}


}
