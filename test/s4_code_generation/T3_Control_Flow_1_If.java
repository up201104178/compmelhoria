package s4_code_generation;

import static org.junit.Assert.*;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.CodeGeneratorUtils;


public class T3_Control_Flow_1_If {



/*checks if the code of a simple IF ELSE statement is well executed */
@Test
public void If_Simple() {
    JasminUtils.testJmm("s4_code_generation/control_flow/SimpleIfElseStat.jmm", "Result: 5\nResult: 8\n");
}


}
