package s4_code_generation;

import static org.junit.Assert.*;

import org.junit.Test;

import jasmin.JasminUtils;
import jasmin.CodeGeneratorUtils;


public class T2_Arithmetic_1_Bytecode_Index {


/*checks if the index for loading a argument is correct (should be 1) */
@Test
public void Bytecode_Index_IloadArg() {
    String jCode = JasminUtils.testJmm("s4_code_generation/arithmetic/ByteCodeIndexes1.jmm", null, false);
    int index = CodeGeneratorUtils.getMehtodExprIndex(jCode,"func", "iload");
    assertTrue("Index should be 1 got index = " + index + " code:\n" + jCode, index == 1);
}

/*checks if the index for loading a variable is correct (should be > 1) */
@Test
public void Bytecode_Index_IloadVar() {
    String jCode = JasminUtils.testJmm("s4_code_generation/arithmetic/ByteCodeIndexes2.jmm", null, false);
    int index = CodeGeneratorUtils.getMehtodExprIndex(jCode,"func", "iload");
    assertTrue("Index should be > 1 got index = " + index + " code:\n" + jCode, index > 1);
}

/*checks if the index for storing a var is correct (should be > 1) */
@Test
public void Bytecode_Index_IstoreVar() {
    String jCode = JasminUtils.testJmm("s4_code_generation/arithmetic/ByteCodeIndexes1.jmm", null, false);
    int index = CodeGeneratorUtils.getMehtodExprIndex(jCode,"func", "istore");
    assertTrue("Index should be > 1 got index = " + index + " code:\n" + jCode, index > 1);
}




}
